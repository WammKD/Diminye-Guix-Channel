(define-module (diminye deklanche)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build        gnu-build-system)
  #:use-module (gnu  packages     shells)
  #:use-module (gnu  packages     xdisorg)
  #:use-module (gnu  packages     xorg))

(define-public deklanche
  (let ([version "1.0.0"])
    (package
      (name              "deklanche")
      (version           version)
      (source            (origin
                           (method    git-fetch)
                           (uri       (git-reference
                                        (url    "https://codeberg.org/tomenzgg/Deklanche/")
                                        (commit                (string-append "v" version))))
                           (file-name (git-file-name name version))
                           (sha256    (base32
                                        "1qn4497d020hh1p18c73n1hlha8rkfs6j9r90p9d6x9ikr56mjzs"))
                           (modules   '((guix build utils)))))
      (build-system      gnu-build-system)
      (arguments         `(#:phases     (modify-phases %standard-phases
                                          (delete 'configure)
                                          (delete 'check))
                           #:make-flags (list (string-append "PREFIX=" (assoc-ref
                                                                         %outputs
                                                                         "out")))))
      (propagated-inputs `(("dash"         ,dash)
                           ("xdotool"   ,xdotool)
                           ("wmctrl"     ,wmctrl)
                           ("xprop"       ,xprop)
                           ("xrandr"     ,xrandr)
                           ("xwininfo" ,xwininfo)))
      (home-page         "https://codeberg.org/tomenzgg/Deklanche/")
      (synopsis          "Dash script to script Aerosnap functionality")
      (description       "Deklanche is a dash script to allow you to snap your
windows around, taking up any half of the screen or even just a corner of the
screen.  It was designed with the intention that the script would be bound with
keyboard shortcuts but all that the script, itself, does is modify the currently
focused window.")
      (license           license:gpl3))))
