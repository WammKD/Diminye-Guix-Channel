(define-module (diminye egzekite)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build        gnu-build-system)
  #:use-module (gnu  packages     xdisorg))

(define-public egzekite
  (package
    (name         "egzekite")
    (version      "1.0.0")
    (source       (origin
                    (method    git-fetch)
                    (uri       (git-reference
                                 (url    "https://codeberg.org/tomenzgg/Egzekite/")
                                 (commit               (string-append "v" version))))
                    (file-name (git-file-name name version))
                    (sha256    (base32
                                 "02avhcwb4f9sxbbsxqqjbm5n80f08327ak4z9nsrrissqrng5jzl"))
                    (modules   '((guix build utils)))))
    (build-system gnu-build-system)
    (arguments    (list #:phases     #~(modify-phases %standard-phases
                                         (add-after 'unpack 'set-inputs
                                           (lambda* (#:key inputs #:allow-other-keys)
                                             (substitute* "bin/egzekite"
                                               (("rofi") (search-input-file inputs "/bin/rofi")))))
                                         (delete 'configure)
                                         (delete 'check))
                        #:make-flags #~(list (string-append "PREFIX="
                                                            #$output))))
    (inputs       (list rofi))
    (home-page    "https://codeberg.org/tomenzgg/Egzekite/")
    (synopsis     "Dash script to execute Rofi in \"run\" mode")
    (description  "Egzekite offers a space, powered by @code{rofi}, to enter
commands you'd like to execute without having to open a terminal emulator.

Available commands and past command are offered, as well, to browse through.")
    (license      license:gpl3+)))

(define-public egzekite/wayland
  (package/inherit egzekite
    (name   "egzekite-wayland")
    (inputs (modify-inputs (package-inputs egzekite)
              (replace "rofi" rofi-wayland)))))
