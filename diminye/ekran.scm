(define-module (diminye ekran)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build        gnu-build-system)
  #:use-module (gnu  packages     wm)
  #:use-module (gnu  packages     xdisorg))

(define-public ekran
  (package
    (name              "ekran")
    (version           "1.1.1")
    (source            (origin
                         (method    git-fetch)
                         (uri       (git-reference
                                      (url        "https://codeberg.org/tomenzgg/Ekran/")
                                      (recursive?                                     #t)
                                      (commit                (string-append "v" version))))
                         (file-name (git-file-name name version))
                         (sha256    (base32
                                      "0y9d7bk35i9wmdaw2nn38ka4mi6iysy84min5y09lhm6k971ffcz"))
                         (modules   '((guix build utils)))))
    (build-system      gnu-build-system)
    (arguments         (list #:phases     #~(modify-phases %standard-phases
                                              (add-after 'unpack 'set-inputs
                                                (lambda* (#:key inputs #:allow-other-keys)
                                                  (substitute* "bin/ekran"
                                                    (("grimshot") (search-input-file inputs "/bin/grimshot"))
                                                    (("rofi")     (search-input-file inputs "/bin/rofi")))))
                                              (delete 'configure)
                                              (delete 'check))
                             #:make-flags #~(list (string-append "PREFIX="
                                                                 #$output))))
    (inputs            (list rofi grimshot))
    (home-page         "https://codeberg.org/tomenzgg/Ekran/")
    (synopsis          "Bash script to take a screenshot via @code{grimshot}")
    (description       "Ekran offers a menu, powered by @code{rofi}, to
take a screenshot of the current screen, a specific window, or just a portion
of the screen.")
    (license           license:gpl3)))

(define-public ekran/wayland
  (package/inherit ekran
    (name   "ekran-wayland")
    (inputs (modify-inputs (package-inputs ekran)
              (replace "rofi" rofi-wayland)))))
