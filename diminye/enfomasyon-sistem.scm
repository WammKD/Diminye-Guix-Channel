(define-module (diminye enfomasyon-sistem)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build        gnu-build-system)
  #:use-module (gnu  packages     xdisorg)
  #:use-module (gnu  packages     admin))

(define-public enfòmasyon-sistèm
  (let ([version "1.2.1"])
    (package
      (name         "enfomasyon-sistem")
      (version      version)
      (source       (origin
                      (method    git-fetch)
                      (uri       (git-reference
                                   (url    "https://codeberg.org/tomenzgg/Enf-masyon_Sist-m/")
                                   (commit                        (string-append "v" version))))
                      (file-name (git-file-name name version))
                      (sha256    (base32
                                   "0l594h1345m4wilb5sn2amsh78571bcbscr9p1kc572disgxipcg"))
                      (modules   '((guix build utils)))))
      (build-system gnu-build-system)
      (arguments    (list #:phases     #~(modify-phases %standard-phases
                                           (add-after 'unpack 'set-inputs
                                             (lambda* (#:key inputs #:allow-other-keys)
                                               (substitute* "bin/enfòmasyon-sistèm"
                                                 (("wmctrl") (search-input-file inputs
                                                                                "/bin/wmctrl"))
                                                 (("inxi")   (search-input-file inputs
                                                                                "/bin/inxi")))))
                                           (delete 'configure)
                                           (delete 'check))
                          #:make-flags #~(list (string-append "PREFIX="
                                                              #$output))))
      (inputs       (list  wmctrl inxi-minimal))
      (home-page    "https://codeberg.org/tomenzgg/Enf-masyon_Sist-m/")
      (synopsis     "Bash script to display system information via inxi ")
      (description  "Enfòmasyon Sistèm fullscreens whichever terminal it's
run in and displays system information so the user can be learn about elements
of their computer and operating system.")
      (license      license:gpl3))))
