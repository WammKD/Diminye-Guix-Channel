(define-module (diminye femen)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build        gnu-build-system)
  #:use-module (gnu  packages     xdisorg)
  #:use-module (gnu  packages     fonts))

(define-public fèmen
  (let ([version "1.5.0"])
    (package
      (name              "femen")
      (version           version)
      (source            (origin
                           (method    git-fetch)
                           (uri       (git-reference
                                        (url        "https://codeberg.org/tomenzgg/F-men/")
                                        (recursive?                                     #t)
                                        (commit                (string-append "v" version))))
                           (file-name (git-file-name name version))
                           (sha256    (base32
                                        "1724cqgzjsxdlwpji7jslpff03fxrhwd5sfj0ms5l1zm13wq7rvz"))
                           (modules   '((guix build utils)))))
      (build-system      gnu-build-system)
      (arguments         (list #:phases     #~(modify-phases %standard-phases
                                                (add-after 'unpack 'set-inputs
                                                  (lambda* (#:key inputs #:allow-other-keys)
                                                    (substitute* "bin/fèmen"
                                                      (("wmctrl") (search-input-file inputs "/bin/wmctrl"))
                                                      (("rofi")   (search-input-file inputs "/bin/rofi")))))
                                                (delete 'configure)
                                                (delete 'check))
                               #:make-flags #~(list (string-append "PREFIX="
                                                                   #$output))))
      (propagated-inputs (list font-atui-feather))
      (inputs            (list wmctrl rofi))
      (home-page         "https://codeberg.org/tomenzgg/F-men/")
      (synopsis          "Bash script to display a powermenu by which users can control the powered state of their computer")
      (description       "Fèmen offers a menu, powered by @code{rofi}, to
shutdown, reboot, suspend, hibernate, and logout the system.")
      (license           license:gpl3))))

(define-public fèmen/wayland
  (package/inherit fèmen
    (name   "femen-wayland")
    (inputs (modify-inputs (package-inputs fèmen)
              (replace "rofi" rofi-wayland)))))
