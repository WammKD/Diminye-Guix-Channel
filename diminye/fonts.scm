(define-module (diminye fonts)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system font))

(define-public font-adwaita
  (package
    (name         "font-adwaita")
    (version      "1.0.0")
    (source       (origin
                    (method    git-fetch)
                    (uri       (git-reference
                                 (url    "https://codeberg.org/tomenzgg/adwaita-font/")
                                 (commit                   (string-append "v" version))))
                    (file-name (git-file-name name version))
                    (sha256    (base32
                                 "15vknmj9gb4frzq6jgma3957m8x5dbf9f3jlfvmn2p2lh0bdkwjn"))))
    (build-system font-build-system)
    (home-page    "https://codeberg.org/tomenzgg/adwaita-font/")
    (synopsis     "Iconfont version of Adwaita icons")
    (description  "This package bundles Adwaita icons into a font.")
    (license      license:lgpl3+)))
