(define-module (diminye foto)
  #:use-module ((guix       licenses) #:prefix license:)
  #:use-module (guix       gexp)
  #:use-module (guix       utils)
  #:use-module (guix       packages)
  #:use-module (guix       git-download)
  #:use-module (guix       build-system gnu)
  #:use-module (guix       build        gnu-build-system)
  #:use-module (gnu        packages     xdisorg)
  #:use-module (nondiminye wayland))

(define-public foto
  (package
    (name              "foto")
    (version           "1.3.0")
    (source            (origin
                         (method    git-fetch)
                         (uri       (git-reference
                                      (url    "https://codeberg.org/tomenzgg/Foto/")
                                      (commit           (string-append "v" version))))
                         (file-name (git-file-name name version))
                         (sha256    (base32
                                      "0y92fzs7hdggzqzlnrzpqahl3rsw9919dnji0x0rd7rf2s7pjdmn"))
                         (modules   '((guix build utils)))))
    (build-system      gnu-build-system)
    (arguments         (list #:phases     #~(modify-phases %standard-phases
                                              (add-after 'unpack 'set-inputs
                                                (lambda* (#:key inputs #:allow-other-keys)
                                                  (substitute* "bin/foto"
                                                    (("rofi")   (search-input-file inputs "/bin/rofi")))))
                                              (delete 'configure)
                                              (delete 'check))
                             #:make-flags #~(list (string-append "PREFIX="
                                                                 #$output))))
    (propagated-inputs (list wbg))
    (inputs            (list rofi-wayland))
    (home-page         "https://codeberg.org/tomenzgg/Foto/")
    (synopsis          "Bash script for Wayland to pick and set desktop
wallpapers, using Rofi and Wbg")
    (description       "Foto offers a menu, powered by @code{rofi}, to select
a backgroud for your desktop.")
    (license           license:gpl3+)))
