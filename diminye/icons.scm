(define-module (diminye icons)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build        gnu-build-system)
  #:use-module (gnu  packages     gtk)
  #:use-module (gnu  packages     imagemagick))

(define-public diminye-icon-theme
  (let ([version "1.14.0"])
    (package
      (name          "diminye-icon-theme")
      (version       version)
      (source        (origin
                       (method    git-fetch)
                       (uri       (git-reference
                                    (url        "https://codeberg.org/tomenzgg/Diminye-Icons/")
                                    (commit                        (string-append "v" version))
                                    (recursive?                                             #t)))
                       (file-name (git-file-name name version))
                       (sha256    (base32
                                    "18csx5rrsg4nsscvqvw95gadc22r29jinzbp1wfjczg72qk8risd"))
                       (modules   '((guix build utils)))))
      (build-system  gnu-build-system)
      (arguments     `(#:phases     (modify-phases %standard-phases
                                      (delete 'configure)
                                      (delete 'check))
                       #:make-flags (list (string-append "PREFIX=" (assoc-ref
                                                                     %outputs
                                                                     "out")))))
      (native-inputs `(("gtk+:bin"     ,gtk+ "bin")
                       ("imagemagick" ,imagemagick)))
      (home-page     "https://codeberg.org/tomenzgg/Diminye-Icons/")
      (synopsis      "Icon set for the Diminye Guix distro")
      (description   "Essentially, Papirus for app. icons, an old Elementary
icon theme for anything file-related, and Adwaita for status bars.")
      (license       license:gpl3))))
