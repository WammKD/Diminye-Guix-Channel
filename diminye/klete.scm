(define-module (diminye klete)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build        gnu-build-system)
  #:use-module (gnu  packages     shells)
  #:use-module (gnu  packages     linux)
  #:use-module (gnu  packages     algebra)
  #:use-module (gnu  packages     gnome))

(define-public klète
  (package
    (name         "klete")
    (version      "1.0.0")
    (source       (origin
                    (method    git-fetch)
                    (uri       (git-reference
                                 (url    "https://codeberg.org/tomenzgg/Kl-te/")
                                 (commit            (string-append "v" version))))
                    (file-name (git-file-name name version))
                    (sha256    (base32
                                 "1jlgavr8rn43pc31djzxl1dgxc30grm47sq2whlq3s141k5xy242"))
                    (modules   '((guix build utils)))))
    (build-system gnu-build-system)
    (arguments    (list #:phases     #~(modify-phases %standard-phases
                                         (add-after 'unpack 'set-inputs
                                           (lambda* (#:key inputs #:allow-other-keys)
                                             (substitute* "bin/klète"
                                               (("brightnessctl") (search-input-file inputs
                                                                                     "/bin/brightnessctl"))
                                               (("bc")            (search-input-file inputs
                                                                                     "/bin/bc"))
                                               (("notify-send")   (search-input-file inputs
                                                                                     "/bin/notify-send")))))
                                         (delete 'configure)
                                         (delete 'check))
                        #:make-flags #~(list (string-append "PREFIX="
                                                            #$output))))
    (inputs       (list dash brightnessctl bc libnotify))
    (home-page    "https://codeberg.org/tomenzgg/Kl-te/")
    (synopsis     "Dash script to modify the monitor brightness using brightnessctl")
    (description  "A dash script to modify the monitor brightness using
brightnessctl; it uses libnotify to display brightness adjustments and displays
them as a bar that fills and empties.")
    (license      license:gpl3+)))
