(define-module (diminye kopye)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build        gnu-build-system)
  #:use-module (gnu  packages     xdisorg)
  #:use-module (nondiminye wayland))

(define-public kopye
  (package
    (name         "kopye")
    (version      "1.0.1")
    (source       (origin
                    (method    git-fetch)
                    (uri       (git-reference
                                 (url    "https://codeberg.org/tomenzgg/Kopye/")
                                 (commit            (string-append "v" version))))
                    (file-name (git-file-name name version))
                    (sha256    (base32
                                 "101r9p6siqz239p5k5ibw9i8wfi0mp9hp2arxjipr83hsbab2dss"))
                    (modules   '((guix build utils)))))
    (build-system gnu-build-system)
    (arguments    (list #:phases     #~(modify-phases %standard-phases
                                         (add-after 'unpack 'set-inputs
                                           (lambda* (#:key inputs #:allow-other-keys)
                                             (substitute* "bin/kopye"
                                               [("rofi")    (search-input-file inputs "/bin/rofi")]
                                               [("clipman") (search-input-file inputs "/bin/clipman")])

                                             (substitute* "bin/kopyed"
                                               [("clipman")  (search-input-file inputs "/bin/clipman")]
                                               [("wl-paste") (search-input-file inputs "/bin/wl-paste")])

                                             (substitute* "bin/kopye-efase"
                                               [("rofi")    (search-input-file inputs "/bin/rofi")]
                                               [("clipman") (search-input-file inputs "/bin/clipman")])))
                                         (delete 'configure)
                                         (delete 'check))
                        #:make-flags #~(list (string-append "PREFIX="
                                                            #$output))))
    (inputs       (list clipman wl-clipboard/diminye rofi-wayland))
    (home-page    "https://codeberg.org/tomenzgg/Kopye/")
    (synopsis     "Dash script to wrap the Wayland clipboard manager Clipman")
    (description  "Kopye offers a menu, powered by @code{rofi}, to select
previously copied text so that they're placed at the forefront of your
clipboard.")
    (license      license:gpl3+)))
