(define-module (diminye lanseur)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build        gnu-build-system)
  #:use-module (gnu  packages     base)
  #:use-module (gnu  packages     linux)
  #:use-module (gnu  packages     xdisorg))

(define-public lanseur
  (package
    (name         "lanseur")
    (version      "1.0.0")
    (source       (origin
                    (method    git-fetch)
                    (uri       (git-reference
                                 (url    "https://codeberg.org/tomenzgg/Lanseur/")
                                 (commit              (string-append "v" version))))
                    (file-name (git-file-name name version))
                    (sha256    (base32
                                 "11r3rdg6hj18959axn3c7vajmwfpq8164v3zmyjzv0qzfdq2whah"))
                    (modules   '((guix build utils)))))
    (build-system gnu-build-system)
    (arguments    (list #:phases     #~(modify-phases %standard-phases
                                         (add-after 'unpack 'set-inputs
                                           (lambda* (#:key inputs #:allow-other-keys)
                                             (substitute* "bin/lanseur"
                                               (("ps")      (search-input-file inputs "/bin/ps"))
                                               (("grep")    (search-input-file inputs "/bin/grep"))
                                               (("rofi")    (search-input-file inputs "/bin/rofi"))
                                               (("killall") (search-input-file inputs "/bin/killall")))))
                                         (delete 'configure)
                                         (delete 'check))
                        #:make-flags #~(list (string-append "PREFIX="
                                                            #$output))))
    (inputs       (list grep procps psmisc rofi))
    (home-page    "https://codeberg.org/tomenzgg/Lanseur/")
    (synopsis     "Dash script to launch applications")
    (description  "Lanseur offers a menu, powered by @code{rofi}, to select
available software installed on your computer.")
    (license      license:gpl3+)))

(define-public lanseur/wayland
  (package/inherit lanseur
    (name   "lanseur-wayland")
    (inputs (modify-inputs (package-inputs lanseur)
              (replace "rofi" rofi-wayland)))))
