(define-module (diminye mizajou)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (gnu  packages     shells)
  #:use-module (gnu  packages     gnome))

(define-public mizajou
  (package
    (name         "mizajou")
    (version      "2.1.0")
    (source       (origin
                    (method    git-fetch)
                    (uri       (git-reference
                                 (url    "https://codeberg.org/tomenzgg/Mizajou/")
                                 (commit              (string-append "v" version))))
                    (file-name (git-file-name name version))
                    (sha256    (base32
                                 "1khp11l1q5s7sg78hcbmq6d2lc0ihp84f7hm1iqacxqnpvdrkjs5"))
                    (modules   '((guix build utils)))))
    (build-system gnu-build-system)
    (arguments    (list #:phases     #~(modify-phases %standard-phases
                                         (add-after 'unpack 'set-inputs
                                           (lambda* (#:key inputs #:allow-other-keys)
                                             (substitute* "bin/mizajou"
                                               (("notify-send") (search-input-file inputs
                                                                                   "/bin/notify-send")))))
                                         (delete 'configure)
                                         (delete 'check))
                        #:make-flags #~(list (string-append "PREFIX="
                                                            #$output))))
    (inputs       (list dash libnotify))
    (home-page    "https://codeberg.org/tomenzgg/Mizajou/")
    (synopsis     "Dash script to check for new Guix updates")
    (description  "A Dash script to check for new Guix updates;
it uses @code{libnotify} to display updates' availabity.")
    (license      license:gpl3)))
