(define-module (diminye pefomans)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build        gnu-build-system)
  #:use-module (gnu  packages     shells)
  #:use-module (gnu  packages     fonts))

(define-public pèfòmans
  (package
    (name              "pefomans")
    (version           "1.0.0")
    (source            (origin
                         (method    git-fetch)
                         (uri       (git-reference
                                      (url    "https://codeberg.org/tomenzgg/P-f-mans/")
                                      (commit               (string-append "v" version))))
                         (file-name (git-file-name name version))
                         (sha256    (base32 "09fzqz53gwlqi0s42rkp5n6y5jpih5sinwv5nb6ar677qcxg0iyk"))
                         (modules   '((guix build utils)))))
    (build-system      gnu-build-system)
    (arguments         `(#:phases     (modify-phases %standard-phases
                                        (delete 'configure)
                                        (delete 'check))
                         #:make-flags (list (string-append "PREFIX=" (assoc-ref
                                                                       %outputs
                                                                       "out")))))
    (propagated-inputs (list dash font-atui-feather))
    (home-page         "https://codeberg.org/tomenzgg/P-f-mans/")
    (synopsis          "@command{tint2} executor script for monitoring computer CPU, RAM, and Swap usage")
    (description       "@command{pèfòmans} is a simple @command{dash} script
which was written to be used with @command{tint2}; merely reference it in any
@command{tint2} config. file and it will provide output.  The script uses the
@code{feather} font to provide icons for the result.")
    (license           license:gpl3)))
