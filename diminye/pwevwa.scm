(define-module (diminye pwevwa)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build        gnu-build-system)
  #:use-module (gnu  packages     shells)
  #:use-module (gnu  packages     xdisorg))

(define-public pwevwa
  (let ([version "1.6.0"])
    (package
      (name              "pwevwa")
      (version           version)
      (source            (origin
                           (method    git-fetch)
                           (uri       (git-reference
                                        (url    "https://codeberg.org/tomenzgg/Pwevwa/")
                                        (commit             (string-append "v" version))))
                           (file-name (git-file-name name version))
                           (sha256    (base32
                                        "00a52fg8c4p0pfk7x04348rpk59ll7bv19495rimqa9rcwihy0jz"))
                           (modules   '((guix build utils)))))
      (build-system      gnu-build-system)
      (arguments         `(#:phases     (modify-phases %standard-phases
                                          (delete 'configure)
                                          (delete 'check))
                           #:make-flags (list (string-append "PREFIX=" (assoc-ref
                                                                         %outputs
                                                                         "out")))))
      (propagated-inputs `(("dash"         ,dash)
                           ("wmctrl"     ,wmctrl)))
      (home-page         "https://codeberg.org/tomenzgg/Pwevwa/")
      (synopsis          "A lightweight calendar applet written in C++ using GTK.")
      (description       "Gsimplecal was intentionally made for use with tint2
panel in the openbox environment to be launched upon clock click but, of course,
it will work without it.")
      (license           license:gpl3))))
