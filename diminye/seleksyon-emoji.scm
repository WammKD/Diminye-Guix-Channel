(define-module (diminye seleksyon-emoji)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix build-system gnu)
  #:use-module (gnu  packages autotools)
  #:use-module (gnu  packages freedesktop)
  #:use-module (gnu  packages glib)
  #:use-module (gnu  packages gtk)
  #:use-module (gnu  packages pkg-config)
  #:use-module (gnu  packages xdisorg))

(define-public rofi-emoji
  (package
    (name              "rofi-emoji")
    (version           "3.4.1")
    (source            (origin
                         (method git-fetch)
                         (uri    (git-reference
                                   (url    (string-append "https://github.com/Mange/"
                                                          name
                                                          "/"))
                                   (commit (string-append "v" version))))
                         (sha256 (base32
                                   "0bga0gj948l2xpril7gklm78ngs4l50g44k3iwrmw1sg5din0y34"))))
    (build-system      gnu-build-system)
    (arguments         (list #:phases #~(modify-phases %standard-phases
                                          (add-after 'unpack 'provide-output-instead-of-rofi
                                            (lambda _
                                              (substitute* "configure.ac"
                                                (["\\[rofi_PLUGIN_INSTALL_DIR]=\".*"]
                                                 (string-append "[rofi_PLUGIN_INSTALL_DIR]=\""
                                                                #$output
                                                                "/lib/rofi/\"\n")))))
                                          (add-after 'provide-output-instead-of-rofi
                                                     'patch-adapter-script-xdotool
                                            (lambda* (#:key inputs #:allow-other-keys)
                                              (substitute* "clipboard-adapter.sh"
                                                (["xdotool"]
                                                 (search-input-file inputs
                                                                    "/bin/xdotool"))))))))
    (native-inputs     (list autoconf automake libtool pkg-config))
    (inputs            (list glib cairo xdotool))
    (propagated-inputs (list rofi))
    (synopsis          "Emoji-selector plugin for Rofi")
    (description       "@code{rofi-emoji} is an emoji-selector plugin for Rofi
that copies the selected emoji to the clipboard and can insert it into the
current program window.")
    (home-page         "https://github.com/Mange/rofi-emoji")
    (license           license:expat)))

(define-public rofi-emoji/wayland
  (package/inherit rofi-emoji
    (name              "rofi-emoji-wayland")
    (arguments         (substitute-keyword-arguments (package-arguments rofi-emoji)
                         ((#:phases phases)
                          #~(modify-phases #$phases
                              (replace 'patch-adapter-script-xdotool
                                (lambda* (#:key inputs #:allow-other-keys)
                                  (substitute* "clipboard-adapter.sh"
                                    (("wtype") (search-input-file inputs
                                                                  "/bin/wtype")))))))))
    (inputs            (modify-inputs (package-inputs rofi-emoji)
                         (replace "xdotool" wtype)))
    (propagated-inputs (modify-inputs (package-propagated-inputs rofi-emoji)
                         (replace "rofi" rofi-wayland)))
    (description       (string-append (package-description rofi-emoji)
                                      "\nThis package provides Wayland support by default."))))

(define-public seleksyon-emoji
  (package
    (name         "seleksyon-emoji")
    (version      "1.0.0")
    (source       (origin
                    (method    git-fetch)
                    (uri       (git-reference
                                 (url    "https://codeberg.org/tomenzgg/Seleksyon-Emoji/")
                                 (commit                      (string-append "v" version))))
                    (file-name (git-file-name name version))
                    (sha256    (base32
                                 "0ar768wp1sqyrvkdwrlyi9rx0smnyims322h25n8lk7028zwlb68"))
                    (modules   '((guix build utils)))))
    (build-system gnu-build-system)
    (arguments    (list #:phases     #~(modify-phases %standard-phases
                                         (add-after 'unpack 'set-inputs
                                           (lambda* (#:key inputs #:allow-other-keys)
                                             (substitute* "bin/seleksyon-emoji"
                                               (["rofi"]
                                                (string-append "export ROFI_PLUGIN_PATH=\""
                                                               (assoc-ref inputs "rofi-emoji")
                                                               "/lib/rofi\""
                                                               "\n"
                                                               "export XDG_DATA_DIRS=\""
                                                               (assoc-ref inputs "rofi-emoji")
                                                               "/share${XDG_DATA_DIRS:+:}$XDG_DATA_DIRS\""
                                                               "\n\n"
                                                               (search-input-file inputs
                                                                                  "/bin/rofi"))))))
                                         (delete 'configure)
                                         (delete 'check))
                        #:make-flags #~(list (string-append "PREFIX=" #$output))))
    (inputs       (list rofi-emoji))
    (home-page    "https://codeberg.org/tomenzgg/Egzekite/")
    (synopsis     "Dash script to execute a Rofi Emoji selector")
    (description  "Seleksyon Emoji offers a menu, powered by @code{rofi}
utilizing the Rofi plugin @code{rofi-emoji}, to search for Emojis.

Selected Emojis will be copied to the clipboard and inserted into the active
window.")
    (license      license:gpl3+)))

(define-public seleksyon-emoji/wayland
  (package/inherit seleksyon-emoji
    (name   "seleksyon-emoji-wayland")
    (inputs (modify-inputs (package-inputs seleksyon-emoji)
              (replace "rofi-emoji" rofi-emoji/wayland)))))
