(define-module (diminye services)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages xdisorg)
  #:use-module (srfi srfi-35)

  #:export (home-wl-paste-configuration
            home-wl-paste-service-type

            home-swayidle-configuration
            home-swayidle-service-type))

;;; wl-clipboard
(define-record-type* <home-wl-paste-configuration>
  home-wl-paste-configuration make-home-wl-paste-configuration
  home-wl-paste-configuration?
  (no-newline?        wl-paste-no-newline?                  ;boolean
                      (default #t))
  (primary-clipboard? wl-paste-primary-clipboard?    ;boolean
                      (default #f))
  (watch-command      wl-paste-watch-command      ;file-like | string
                      (default #f))
  (mime-type          wl-paste-mime-type      ;string | #f
                      (default #f))
  (seat-name          wl-paste-seat-name      ;string | #f
                      (default #f)))

(define (home-wl-paste-shepherd-services config)
  (let ([no-newline?        (wl-paste-no-newline?        config)]
        [primary-clipboard? (wl-paste-primary-clipboard? config)]
        [watch-command      (wl-paste-watch-command      config)]
        [mime-type          (wl-paste-mime-type          config)]
        [seat-name          (wl-paste-seat-name          config)])
    (list (shepherd-service
            (provision '(wl-paste))
            (documentation "Run wl-paste in watch mode.")
            (start #~(make-forkexec-constructor
                       (append (list #$(file-append wl-clipboard "/bin/wl-paste"))
                               (if #$no-newline?        (list "-n") (list))
                               (if #$primary-clipboard? (list "-p") (list))
                               (if #$watch-command
                                   (list "-w" #$watch-command)
                                 (raise
                                   (condition
                                     (&message
                                       (message
                                         "watch command must be provided")))))
                               (if #$mime-type
                                   (list "-t" #$mime-type)
                                 (list))
                               (if #$seat-name
                                   (list "-s" #$seat-name)
                                 (list)))))
            (stop #~(make-kill-destructor))))))

(define home-wl-paste-service-type
  (service-type (name          'home-wl-paste)
                (extensions    (list (service-extension home-shepherd-service-type
                                                        home-wl-paste-shepherd-services)))
                (default-value (home-wl-paste-configuration))
                (description   "Run wl-paste, a clipboard utility, with a watch command.")))



;;; Swayidle
(define-record-type* <home-swayidle-configuration>
  home-swayidle-configuration make-home-swayidle-configuration
  home-swayidle-configuration?
  (timeout-interval-1   swayidle-timeout-interval-1   ;integer
                        (default 15))
  (timeout-command-1    swayidle-timeout-command-1    ;file-like | string
                        (default #f))
  (resume-command-1     swayidle-resume-command-1     ;file-like | string
                        (default #f))
  (timeout-interval-2   swayidle-timeout-interval-2   ;integer
                        (default 15))
  (timeout-command-2    swayidle-timeout-command-2    ;file-like | string
                        (default #f))
  (resume-command-2     swayidle-resume-command-2     ;file-like | string
                        (default #f))
  (timeout-interval-3   swayidle-timeout-interval-3   ;integer
                        (default 15))
  (timeout-command-3    swayidle-timeout-command-3    ;file-like | string
                        (default #f))
  (resume-command-3     swayidle-resume-command-3     ;file-like | string
                        (default #f))
  (timeout-interval-4   swayidle-timeout-interval-4   ;integer
                        (default 15))
  (timeout-command-4    swayidle-timeout-command-4    ;file-like | string
                        (default #f))
  (resume-command-4     swayidle-resume-command-4     ;file-like | string
                        (default #f))
  (before-sleep-command swayidle-before-sleep-command ;file-like | string
                        (default #f))
  (after-resume-command swayidle-after-resume-command ;file-like | string
                        (default #f))
  (lock-command         swayidle-lock-command         ;file-like | string
                        (default #f))
  (unlock-command       swayidle-unlock-command       ;file-like | string
                        (default #f))
  (idlehint-timeout     swayidle-idlehint-timeout     ;integer
                        (default #f)))

(define (home-swayidle-shepherd-services config)
  (let ([timeout-interval-1   (swayidle-timeout-interval-1   config)]
        [timeout-command-1    (swayidle-timeout-command-1    config)]
        [resume-command-1     (swayidle-resume-command-1     config)]
        [timeout-interval-2   (swayidle-timeout-interval-2   config)]
        [timeout-command-2    (swayidle-timeout-command-2    config)]
        [resume-command-2     (swayidle-resume-command-2     config)]
        [timeout-interval-3   (swayidle-timeout-interval-3   config)]
        [timeout-command-3    (swayidle-timeout-command-3    config)]
        [resume-command-3     (swayidle-resume-command-3     config)]
        [timeout-interval-4   (swayidle-timeout-interval-4   config)]
        [timeout-command-4    (swayidle-timeout-command-4    config)]
        [resume-command-4     (swayidle-resume-command-4     config)]
        [before-sleep-command (swayidle-before-sleep-command config)]
        [after-resume-command (swayidle-after-resume-command config)]
        [lock-command         (swayidle-lock-command         config)]
        [unlock-command       (swayidle-unlock-command       config)]
        [idlehint-timeout     (swayidle-idlehint-timeout     config)])
    (list (shepherd-service
            (provision '(swayidle))
            (modules '((srfi srfi-1) (srfi srfi-26)))
            (documentation "Run swayidle.")
            (start #~(make-forkexec-constructor
                       (append (list #$(file-append swayidle "/bin/swayidle"))
                               (if (and #$timeout-interval-1 #$timeout-command-1)
                                   (append (list "timeout" (number->string #$timeout-interval-1)
                                                           #$timeout-command-1)
                                           (if #$resume-command-1
                                               (list "resume" #$resume-command-1)
                                             (list)))
                                 (list))
                               (if (and #$timeout-interval-2 #$timeout-command-2)
                                   (append (list "timeout" (number->string #$timeout-interval-2)
                                                           #$timeout-command-2)
                                           (if #$resume-command-2
                                               (list "resume" #$resume-command-2)
                                             (list)))
                                 (list))
                               (if (and #$timeout-interval-3 #$timeout-command-3)
                                   (append (list "timeout" (number->string #$timeout-interval-3)
                                                           #$timeout-command-3)
                                           (if #$resume-command-3
                                               (list "resume" #$resume-command-3)
                                             (list)))
                                 (list))
                               (if (and #$timeout-interval-4 #$timeout-command-4)
                                   (append (list "timeout" (number->string #$timeout-interval-4)
                                                           #$timeout-command-4)
                                           (if #$resume-command-4
                                               (list "resume" #$resume-command-4)
                                             (list)))
                                 (list))
                               (if #$before-sleep-command
                                   (list "before-sleep" #$before-sleep-command)
                                 (list))
                               (if #$after-resume-command
                                   (list "after-resume" #$after-resume-command)
                                 (list))
                               (if #$lock-command
                                   (list "lock" #$lock-command)
                                 (list))
                               (if #$unlock-command
                                   (list "unlock" #$unlock-command)
                                 (list))
                               (if #$idlehint-timeout
                                   (list "idlehint" (number->string #$idlehint-timeout))
                                 (list)))
                       #:log-file (string-append (or (getenv "XDG_STATE_HOME")
                                                     (format #f
                                                             "~a/.local/state"
                                                             (getenv "HOME")))
                                                 "/log/swayidle.log")
                       #:environment-variables (cons (string-append "WAYLAND_DISPLAY="
                                                                    (if (getenv "WAYLAND_DISPLAY")
                                                                        (getenv "WAYLAND_DISPLAY")
                                                                      "wayland-1"))
                                                     (cons (string-append "DISPLAY=" "0:0")
                                                           (remove (cut string-prefix? "DISPLAY=" <>)
                                                                   (default-environment-variables))))))
            (stop #~(make-kill-destructor))))))

(define home-swayidle-service-type
  (service-type (name          'home-swayidle)
                (extensions    (list (service-extension home-shepherd-service-type
                                                        home-swayidle-shepherd-services)))
                (default-value (home-swayidle-configuration))
                (description   "Run swayidle, an idle management daemon for Wayland compositors.")))
