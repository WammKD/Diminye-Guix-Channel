(define-module (diminye themes)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build        gnu-build-system)
  #:use-module (gnu  packages     gnome))

(define-public openbox-adwaita-theme
  (package
    (name              "openbox-adwaita-theme")
    (version           "1.1.0")
    (source            (origin
                         (method    git-fetch)
                         (uri       (git-reference
                                      (url    "https://codeberg.org/tomenzgg/Openbox-Adwaita/")
                                      (commit                      (string-append "v" version))))
                         (file-name (git-file-name name version))
                         (sha256    (base32
                                      "0ydrl6r2v2k227h905zgsgdsx4784nym0brrbm6yavdjaq8rvc2a"))
                         (modules   '((guix build utils)))))
    (build-system      gnu-build-system)
    (arguments         `(#:phases     (modify-phases %standard-phases
                                        (delete 'configure)
                                        (delete 'check))
                         #:make-flags (list (string-append "PREFIX=" (assoc-ref
                                                                       %outputs
                                                                       "out")))))
    (propagated-inputs (list gnome-themes-standard))
    (home-page         "https://codeberg.org/tomenzgg/Openbox-Adwaita/")
    (synopsis          "Adwaita Theme for the Openbox Window Manager")
    (description       "This package provides a theme which will work with the
@code{openbox} window manager and emulate the @code{adwaita} theme by Gnome.")
    (license           license:gpl3)))

(define-public openbox-adwaita-hidpi-theme
  (package
    (inherit   openbox-adwaita-theme)
    (name      "openbox-adwaita-hidpi-theme")
    (arguments `(#:phases     (modify-phases %standard-phases
                                (delete 'configure)
                                (delete 'check))
                 #:make-flags (list
                                (string-append "PREFIX=" (assoc-ref
                                                           %outputs
                                                           "out"))
                                "HIDPI=true")))))
