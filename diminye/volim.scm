(define-module (diminye volim)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build        gnu-build-system)
  #:use-module (gnu  packages     shells)
  #:use-module (gnu  packages     pulseaudio)
  #:use-module (gnu  packages     algebra)
  #:use-module (gnu  packages     gnome)
  #:use-module (gnu  packages     xiph)
  #:use-module (gnu  packages     libcanberra))

(define-public volim
  (package
    (name         "volim")
    (version      "1.3.0")
    (source       (origin
                    (method    git-fetch)
                    (uri       (git-reference
                                 (url    "https://codeberg.org/tomenzgg/Volim/")
                                 (commit            (string-append "v" version))))
                    (file-name (git-file-name name version))
                    (sha256    (base32
                                 "09w33sig74lcp2j9xcds3spjyf550x1lp736zmr5vz2qizazkkdh"))
                    (modules   '((guix build utils)))))
    (build-system gnu-build-system)
    (arguments    (list #:phases     #~(modify-phases %standard-phases
                                         (add-after 'unpack 'set-inputs
                                           (lambda* (#:key inputs #:allow-other-keys)
                                             (substitute* "bin/volim"
                                               (("pulsemixer")  (search-input-file inputs
                                                                                   "/bin/pulsemixer"))
                                               (("bc")          (search-input-file inputs
                                                                                   "/bin/bc"))
                                               (("ogg123")      (search-input-file inputs
                                                                                   "/bin/ogg123"))
                                               (("/usr")        (assoc-ref         inputs
                                                                                   "sound-theme-freedesktop"))
                                               (("notify-send") (search-input-file inputs
                                                                                   "/bin/notify-send")))))
                                         (delete 'configure)
                                         (delete 'check))
                        #:make-flags #~(list (string-append "PREFIX="
                                                            #$output))))
    (inputs       (list dash      pulsemixer   bc
                        libnotify vorbis-tools sound-theme-freedesktop))
    (home-page    "https://codeberg.org/tomenzgg/Volim/")
    (synopsis     "Dash script to modify the volume using pulsemixer")
    (description  "A dash script to modify the volume using pulsemixer;
it uses libnotify to display volume adjustments and displays them as a bar that
fills and empties.")
    (license      license:gpl3)))
