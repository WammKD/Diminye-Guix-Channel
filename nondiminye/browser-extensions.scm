(define-module (nondiminye browser-extensions)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (gnu  build        chromium-extension)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (gnu  packages     compression))

(define bypass-paywalls
  (package
    (name          "bypass-paywalls")
    (version       "1.8.0")
    (source        (origin
                     (method git-fetch)
                      (uri    (git-reference
                               (url    "https://github.com/iamadamdev/bypass-paywalls-chrome")
                               (commit (string-append "v" version))))
                     (sha256 (base32
                               "103917jsn6py3wi4gw216rs6winrd1wkkag1zqxczkf2y7c9bndb"))))
    (build-system  gnu-build-system)
    (native-inputs (list p7zip))
    (outputs       '("xpi" "firefox" "chromium"))
    (arguments     (list #:phases #~(modify-phases (map (lambda (phase)
                                                          (assq phase %standard-phases))
                                                        '(set-paths unpack patch-source-shebangs))
                                      (add-after 'patch-source-shebangs 'prepare-build
                                        (lambda _
                                          (chdir "build")

                                          (mkdir-p "firefox")
                                          (mkdir-p "chromium")

                                          (substitute* "build.sh"
                                            (("# Remove temp files")
                                             "cp $FF_FILES firefox
mv firefox/temp-ff-manifest.json firefox/manifest.json
mv firefox/temp-background.js    firefox/background.js
mv firefox/temp-options.html     firefox/options.html
mv firefox/temp-popup.html       firefox/popup.html

cp $CH_FILES chromium
mv chromium/temp-chrome-manifest.json chromium/manifest.json
mv chromium/temp-background.js        chromium/background.js
mv chromium/temp-options.html         chromium/options.html
mv chromium/temp-popup.html           chromium/popup.html

# Remove temp files"))))
                                      (add-after 'prepare-build 'build
                                        (lambda _
                                          (invoke "./build.sh")))
                                      (add-after 'build 'install
                                        (lambda* (#:key outputs #:allow-other-keys)
                                          (let ((firefox (assoc-ref outputs "firefox"))
                                                (xpi (assoc-ref outputs "xpi"))
                                                (chromium (assoc-ref outputs "chromium")))
                                            (install-file "output/bypass-paywalls.xpi"
                                                          (string-append xpi "/lib/mozilla/extensions"))
                                            (copy-recursively "firefox" firefox)
                                            (copy-recursively "chromium" chromium)))))))
    (home-page         "https://github.com/iamadamdev/bypass-paywalls-chrome")
    (synopsis          "Widevine library for Linux")
    (description       "I'm tired.")
    (license           license:gpl3+)))

(define-public bypass-paywalls/chromium
  (make-chromium-extension bypass-paywalls "chromium"))

(define vimium
  (package
    (name "vimium")
    (version "1.67.2")
    (home-page "https://github.com/philc/vimium")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url home-page)
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1nr49lv562zkhzk7c3dqhmm0pwmina7p718rcy7ap98gxp1llvfh"))))
    (build-system copy-build-system)
    (synopsis "Operate your web browser with Vim-like key bindings")
    (description
     "Vimium is a browser extension that provides keyboard-based navigation and
control of the web in the spirit of the Vim editor.")
    (license license:expat)))

(define-public vimium/chromium
  (make-chromium-extension vimium))
