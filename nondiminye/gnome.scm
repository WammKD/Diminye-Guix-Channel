(define-module (nondiminye gnome)
  #:use-module (gnu  packages gnome)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix utils))

(define-public gnome-desktop/next
  (package/inherit gnome-desktop
    (name    "gnome-desktop-next")
    (version "44.0")
    (source  (origin
               (method url-fetch)
               (uri    (string-append "mirror://gnome/sources/"
                                      "gnome-desktop"           "/"
                                      (version-major version)   "/"
                                      "gnome-desktop"           "-"
                                      version                   ".tar.xz"))
               (sha256 (base32
                         "0hlxqprraiwnccf98dykbhx80j31c3scdi7i3jy19fl4bms77is2"))))))

(define-public gnome/next
  (package/inherit gnome
    (name              "gnome-next")
    (version           (package-version gnome-desktop/next))
    (propagated-inputs (modify-inputs (package-propagated-inputs gnome)
                         (replace "gnome-desktop" gnome-desktop/next)))))
