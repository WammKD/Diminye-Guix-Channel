(define-module (nondiminye guile-wayland)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix     download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix packages)
  #:use-module (gnu  packages)
  #:use-module (gnu  packages autotools)
  #:use-module (gnu  packages bash)
  #:use-module (gnu  packages freedesktop)
  #:use-module (gnu  packages gettext)
  #:use-module (gnu  packages gl)
  #:use-module (gnu  packages gtk)
  #:use-module (gnu  packages guile)
  #:use-module (gnu  packages guile-xyz)
  #:use-module (gnu  packages ibus)
  #:use-module (gnu  packages pkg-config)
  #:use-module (gnu  packages texinfo)
  #:use-module (gnu  packages wm)
  #:use-module (gnu  packages xdisorg)
  #:use-module (gnu  packages xml)
  #:use-module (gnu  packages xorg))

(define-public util572
  (let ([commit   "beaf1baef3c3af697ce6f92ec89dadbf044be001"]
        [version                                     "0.0.0"]
        [revision                                        "0"])
    (package
      (name          "util572")
      (version       (git-version version revision commit))
      (source        (origin
                       (method    git-fetch)
                       (uri       (git-reference
                                    (url        (string-append "https://github.com/Z572/"
                                                               name "/"))
                                    (recursive? #t)
                                    (commit     commit)))
                       (file-name (git-file-name name version))
                       (sha256    (base32
                                    "192grp30wbhnqcwr5ynwsk6k8xil61gmbwpwmad1fggcqihbbm2m"))))
      (build-system  gnu-build-system)
      (arguments     (list #:make-flags #~(list "GUILE_AUTO_COMPILE=0")))
      (native-inputs (list pkg-config autoconf automake texinfo))
      (inputs        (list guile-3.0))
      (home-page     "")
      (synopsis      "")
      (description   "")
      (license license:gpl3+))))

(define-public guile-bytestructure-class
  (package
    (name              "guile-bytestructure-class")
    (version           "0.2.0")
    (source            (origin
                         (method    git-fetch)
                         (uri       (git-reference
                                      (url        (string-append "https://github.com/Z572/"
                                                                 name "/"))
                                      (recursive? #t)
                                      (commit     (string-append "v" version))))
                         (file-name (git-file-name name version))
                         (sha256    (base32
                                      "0y3sryy79arp3f5smyxn8w7zra3j4bb0qdpl1p0bld3jicc4s86a"))))
    (build-system      gnu-build-system)
    (arguments         (list #:make-flags #~'("GUILE_AUTO_COMPILE=0")))
    (native-inputs     (list autoconf automake pkg-config))
    (inputs            (list guile-3.0))
    (propagated-inputs (list guile-bytestructures))
    (synopsis          "bytestructure and goops")
    (description       "This package combines bytestructure with goops,
and provide 4 new bytestructure-descriptor:
bs:unknow, cstring-pointer*, bs:enum, stdbool.")
    (home-page         "https://github.com/Z572/guile-bytestructure-class")
    (license           license:gpl3+)))

(define-public guile-libinput
  (let ([commit   "afda6be5c47591d424cf685354375dd1460c4314"]
        [version                                     "0.0.0"]
        [revision                                        "0"])
    (package
      (name              "guile-libinput")
      (version           (git-version version revision commit))
      (source            (origin
                           (method    git-fetch)
                           (uri       (git-reference
                                        (url        (string-append "https://github.com/Z572/"
                                                                   name "/"))
                                        (recursive? #t)
                                        (commit     commit)))
                           (file-name (git-file-name name version))
                           (sha256    (base32
                                        "01ldi7sayc5vzy4ia9n8k0xbhdx19yxjscpzf37vr1y825r2ql49"))))
      (build-system      gnu-build-system)
      (arguments         (list #:make-flags #~(list "GUILE_AUTO_COMPILE=0")))
      (native-inputs     (list autoconf automake pkg-config))
      (inputs            (list guile-3.0 libinput))
      (propagated-inputs (list guile-bytestructure-class guile-bytestructures))
      (synopsis          "")
      (description       "")
      (home-page         "")
      (license           license:gpl3+))))

(define-public guile-xkbcommon
  (let ([commit   "ffe427d21b61f860000d3d4e1808a0926104285e"]
        [version                                     "0.0.0"]
        [revision                                        "0"])
    (package
      (name              "guile-xkbcommon")
      (version           (git-version version revision commit))
      (source            (origin
                           (method    git-fetch)
                           (uri       (git-reference
                                        (url        (string-append "https://github.com/Z572/"
                                                                   name "/"))
                                        (recursive? #t)
                                        (commit     commit)))
                           (file-name (git-file-name name version))
                           (sha256    (base32
                                        "0lxaw3vyphjy2sm1jnks8a5njvvrymyz66j8v7xanaj7z8nd2k91"))))
      (build-system      gnu-build-system)
      (arguments         (list #:make-flags #~(list "GUILE_AUTO_COMPILE=0")))
      (native-inputs     (list autoconf automake pkg-config))
      (inputs            (list guile-3.0
                               libxkbcommon
                               ;;; xkbregistry pc file require
                               libxml2))
      (propagated-inputs (list guile-bytestructure-class guile-bytestructures))
      (synopsis          "")
      (description       "")
      (home-page         "")
      (license           license:gpl3+))))

(define-public guile-wayland
  (let ([commit   "0f19d60aff9f490d198979f1091c62be601de007"]
        [version                                     "0.0.0"]
        [revision                                        "0"])
    (package
      (name              "guile-wayland")
      (version           (git-version version revision commit))
      (source            (origin
                           (method    git-fetch)
                           (uri       (git-reference
                                        (url        (string-append "https://github.com/guile-wayland/"
                                                                   name "/"))
                                        (recursive? #t)
                                        (commit     commit)))
                           (file-name (git-file-name name version))
                           (sha256    (base32
                                        "0zygbpl005yv86vpzhsf8inqznn6r4grpaal2nmw6619hv8xz8ks"))))
      (build-system      gnu-build-system)
      (arguments         (list #:make-flags '(list "GUILE_AUTO_COMPILE=0")
                               #:phases     #~(modify-phases %standard-phases
                                                (add-after 'build 'load-extension
                                                  (lambda* (#:key outputs #:allow-other-keys)
                                                    (substitute* (find-files "." ".*\\.scm")
                                                      (("\\(load-extension \"libguile-wayland\" *\"(.*)\"\\)" _ o)
                                                       (string-append
                                                         (object->string
                                                           `(or (false-if-exception (load-extension "libguile-wayland" ,o))
                                                                (load-extension ,(string-append (assoc-ref outputs "out")
                                                                                                "/lib/libguile-wayland.so")
                                                                                ,o)))))))))))
      (native-inputs     (list autoconf automake libtool pkg-config texinfo))
      (inputs            (list guile-3.0 wayland))
      (propagated-inputs (list guile-bytestructure-class guile-bytestructures))
      (synopsis          "")
      (description       "")
      (home-page         "")
      (license           license:gpl3+))))

(define-public guile-wlroots
  (let ([commit   "fc57f63933b02bdee8775e8a0c9dfe04be193d1a"]
        [version                                     "0.0.0"]
        [revision                                        "0"])
    (package
      (name              "guile-wlroots")
      (version           (git-version version revision commit))
      (source            (origin
                           (method    git-fetch)
                           (uri       (git-reference
                                        (url        (string-append "https://github.com/Z572/"
                                                                   name "/"))
                                        (recursive? #t)
                                        (commit     commit)))
                           (file-name (git-file-name name version))
                           (sha256    (base32
                                        "1qp8cbs88my4r42ic7nvaqy015zrjhasz650i542mrjm770iyy6z"))))
      (build-system      gnu-build-system)
      (arguments         (list #:make-flags '(list "GUILE_AUTO_COMPILE=0")
                               #:phases     #~(modify-phases %standard-phases
                                                (add-after 'build 'load-extension
                                                  (lambda* (#:key outputs #:allow-other-keys)
                                                    (substitute* (find-files "." ".*\\.scm")
                                                      (("\\(load-extension \"libguile-wlroots\" *\"(.*)\"\\)" _ o)
                                                       (string-append
                                                         (object->string
                                                           `(or (false-if-exception (load-extension "libguile-wlroots" ,o))
                                                                (load-extension ,(string-append (assoc-ref outputs "out")
                                                                                                "/lib/libguile-wlroots.so")
                                                                                ,o)))))))))))
      (native-inputs     (list autoconf automake libtool pkg-config texinfo))
      (inputs            (list guile-3.0 wlroots))
      (propagated-inputs (list util572
                               guile-bytestructures
                               guile-bytestructure-class
                               guile-libinput
                               guile-xkbcommon
                               guile-wayland))
      (synopsis          "")
      (description       "")
      (home-page         "")
      (license           license:gpl3+))))
