(define-module (nondiminye guile)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu  packages autotools)
  #:use-module (gnu  packages compression)
  #:use-module (gnu  packages curl)
  #:use-module (gnu  packages databases)
  #:use-module (gnu  packages gettext)
  #:use-module (gnu  packages guile)
  #:use-module (gnu  packages guile-xyz))

(define-public guile-dbi-3
  (package/inherit guile-dbi
    (native-inputs     (modify-inputs (package-native-inputs     guile-dbi)
                         (prepend libltdl)))
    (propagated-inputs (modify-inputs (package-propagated-inputs guile-dbi)
                         (replace "guile" guile-3.0)))))

(define-public guile-dbd-mysql-3
  (package
    (inherit guile-dbi-3)
    (name "guile-dbd-mysql")
    (arguments
     (substitute-keyword-arguments (package-arguments guile-dbi-3)
       ((#:phases phases)
        `(modify-phases ,phases
           (replace 'chdir
             (lambda _
               ;; The upstream Git repository contains all the code, so change
               ;; to the directory specific to guile-dbd-mysql.
               (chdir "guile-dbd-mysql")))
           (add-after 'chdir 'patch-src
             (lambda _
               (substitute* "configure.ac"
                 (("mariadbclient") "mariadb"))
               (substitute* "src/guile-dbd-mysql.c"
                 (("<mariadb/") "<mysql/"))))
           (delete 'patch-extension-path)))))
    (inputs (modify-inputs (package-inputs guile-dbi-3)
              (prepend `(,mariadb "dev")
                       `(,mariadb "lib") zlib)))
    (native-inputs
     (modify-inputs (package-native-inputs guile-dbi-3)
       (prepend guile-dbi-3 ; only required for headers
                )))
    (synopsis "Guile DBI driver for MySQL")
    (description "@code{guile-dbi} is a library for Guile that provides a
convenient interface to SQL databases.  This package implements the interface
for MySQL.")
    (license license:gpl2+)))

(define-public artanis-latest
  (let ([version                                     "0.5.1"]
        [commit   "22b4b399e4f7c10570050fccbb43acf6965c307b"]
        [revision                                        "4"])
    (package/inherit artanis
      (name              "artanis-latest")
      (version           (git-version version revision commit))
      (source            (origin
                           (method  git-fetch)
                           (uri     (git-reference (commit commit)
                                                   (url    "https://gitlab.com/hardenedlinux/artanis")))
                           (sha256  (base32
                                      "0804vag6ppvmcamk1ynps50qmw7bhqcm7704vw5jr0032rg98df6"))
                           (modules (origin-modules (package-source artanis)))
                           (snippet (origin-snippet (package-source artanis)))))
      (propagated-inputs (modify-inputs (package-propagated-inputs artanis)
                           (prepend guile-curl guile-dbi-3 guile-dbd-mysql-3)))
      (native-inputs     (modify-inputs (package-native-inputs artanis)
                           (prepend autoconf
                                    automake
                                    gettext-minimal))))))

(define-public artanis-and-mariadb
  (package/inherit artanis
    (name   "artanis-and-mariadb")
    (inputs (modify-inputs (package-inputs artanis)
              (prepend guile-dbi guile-dbd-mysql)))))
