(define-module (nondiminye makemkv-oss)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (gnu  packages     pkg-config)
  #:use-module (gnu  packages     compression)
  #:use-module (gnu  packages     tls)
  #:use-module (gnu  packages     xml)
  #:use-module (gnu  packages     video)
  #:use-module (gnu  packages     qt))

(define-public makemkv-oss
  (package
    (name          "makemkv-oss")
    (version       "1.17.2")
    (source        (origin
                     (method url-fetch)
                     (uri    (string-append "https://www.makemkv.com/download/"
                                            name "-" version ".tar.gz"))
                     (sha256 (base32
                               "191d7ywr07x2hf51la8jfqpxzdvm5833brc3hzfg1pi37yxqlgx8"))))
    (build-system  gnu-build-system)
    (arguments     '(#:tests? #f
                     #:phases (modify-phases %standard-phases
                                (add-before 'configure 'set-ldconfig-cache
                                  (lambda* (#:key inputs #:allow-other-keys)
                                    (substitute* "Makefile.in"
                                      [("ldconfig") "ldconfig -C /tmp/ld.so.cache~"])

                                    (substitute* "makemkvgui/src/api_linux.cpp"
                                      [("\"/usr/local/bin\",") (string-append "\""
                                                                            (string-join (map cdr inputs)
                                                                                         "\", \"")
                                                                            "\",")]))))))
    (native-inputs (list pkg-config))
    (inputs        (list zlib openssl expat ffmpeg qtbase-5))
    (home-page     "https://www.makemkv.com/")
    (synopsis      "Helper files for MakeMKV")
    (description   "MakeMKV allows for creating MKV files from the chapters of a
DVD.

This package provides the MakeMKV GUI, libmakemkv multiplexer library, and
libdriveio MMC drive interrogation library")
    (license       license:lgpl2.1)))

(define-public makemkv-oss/wayland
  (package/inherit makemkv-oss
    (name   "makemkv-oss-wayland")
    (inputs (modify-inputs (package-inputs makemkv-oss)
              (replace "qtbase" qtwayland-5)))))
