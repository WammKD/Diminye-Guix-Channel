(define-module (nondiminye mega)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (gnu  packages     pkg-config)
  #:use-module (gnu  packages     autotools)
  #:use-module (gnu  packages     crypto)
  #:use-module (gnu  packages     compression)
  #:use-module (gnu  packages     sqlite)
  #:use-module (gnu  packages     tls)
  #:use-module (gnu  packages     adns)
  #:use-module (gnu  packages     curl)
  #:use-module (gnu  packages     image)
  #:use-module (gnu  packages     readline)
  #:use-module (gnu  packages     crypto)
  #:use-module (gnu  packages     qt)
  #:use-module (gnu  packages     video)
  #:use-module (gnu  packages     libevent)
  #:use-module (gnu  packages     linux)
  #:use-module (gnu  packages     photo)
  #:use-module (gnu  packages     xorg)
  #:use-module (gnu  packages     check)
  #:use-module (gnu  packages     icu4c))

(define-public mega-sdk
  (package
    (name          "mega-sdk")
    (version       "5.2.1")
    (source        (origin
                     (method    git-fetch)
                     (uri       (git-reference
                                  (url    "https://github.com/meganz/sdk/")
                                  (commit (string-append "v" version))))
                     (file-name (git-file-name name version))
                     (sha256    (base32
                                  "00n6ka3gkxkpq0i075iw8rxm7vkkhna73am0havvkabs3prjshx8"))
                     (modules   '((guix build utils)))))
    (build-system  gnu-build-system)
    (native-inputs (list autoconf automake libtool googletest))
    (inputs        (list c-ares    crypto++ curl
                         freeimage icu4c    libsodium
                         openssl   readline sqlite    zlib))
    (arguments
    `(#:phases
      (modify-phases %standard-phases
        (add-before 'bootstrap 'remove-tests-that-require-a-mega-account
          (lambda _
            (substitute* "tests/include.am"
              (("TESTS = tests/test_unit .+") "TESTS = tests/test_unit")
              ;; test_integration related
              (("tests_test_integration_SOURCES = \\\\") "")
              (("    tests/gtest_common\\.cpp \\\\") "")
              (("    tests/integration/main\\.cpp \\\\") "")
              (("    tests/integration/SdkTest_test\\.cpp \\\\") "")
              (("    tests/integration/Sync_test\\.cpp") "")
              (("tests_test_integration_CXXFLAGS = .+") "")
              (("tests_test_integration_LDADD = .+") ""))

            (substitute* "contrib/QtCreator/MEGAtests/MEGAtests.pro"
              (("SUBDIRS [+]= MEGAtest_integration") "")
              (("SUBDIRS [+]= MEGAtest_purge_account") "")
              (("    SUBDIRS [+]= MEGAtest_integration_fsevents_loader") "")
              (("    MEGAtest_integration_fsevents_loader\\.depends = .+") "")))))))
    (home-page "https://mega.nz/sdk")
    (synopsis "SDK for the MEGA service, offered by MEGA Limited")
    (description "A low-level SDK for the MEGA service which powers the
file-sharing site.  This package provides two executables:
@itemize
@item megacli: a command-line tool that allows usage of all SDK features
@item megasimplesync: a command line tool that allows usage of the
synchronization engine
@end itemize")
    (license license:bsd-2)))

(define-public megasync
  (let ([version "5.2.1.0"])
    (package
      (name              "megasync")
      (version           version)
      (source            (origin
                           (method    git-fetch)
                           (uri       (git-reference
                                        (url "https://github.com/meganz/MEGAsync/")
                                        (commit (string-append "v" version "_Linux"))
                                        ;; Building the SDK fails, normally, as it has
                                        ;; to download other dependencies during the
                                        ;; build but the MEGAsync build fails without
                                        ;; a recursive clone due to missing header
                                        ;; files present in the SDK source; recursively
                                        ;; cloning solves the latter issue while
                                        ;; providing the Guix-packaged MEGA SDK solves
                                        ;; MEGAsync needing the SDK as a dependency
                                        (recursive? #t)))  ; for SDK
                           (file-name (git-file-name name version))
                           (sha256    (base32
                                        "1nrk4c1mr8d3g6flpzia6glyqj5mdsip0f07h1my1k1x4lqfp2vz"))))
      (build-system      gnu-build-system)
      (propagated-inputs (list xrdb))
      (inputs            (list mega-sdk qtbase-5 qtdeclarative-5 qtsvg-5 qtx11extras icu4c))
      (native-inputs     (list qttools      curl   crypto++ libsodium
                               openssl      c-ares sqlite   freeimage
                               libmediainfo libuv  eudev    libraw))
      (arguments         `(#:phases (modify-phases %standard-phases
                                      (replace 'configure
                                        (lambda* (#:key outputs #:allow-other-keys)
                                          (delete-file-recursively "build")
                                          (chdir "src")

                                          (substitute* "MEGASync/MEGASync.pro"
                                            [("#        PREFIX = /usr")                     "#        PREFIX = /"]
                                            [("#    ")                                                     "    "]
                                            [("    Uncomment the following if") "#    Uncomment the following if"])

                                          (invoke "qmake"    "MEGA.pro")
                                          (invoke "lrelease" "MEGASync/gui/translations/MEGASyncStrings_ar.ts")
                                          (invoke "lrelease" "MEGASync/gui/translations/MEGASyncStrings_de.ts")
                                          (invoke "lrelease" "MEGASync/gui/translations/MEGASyncStrings_en.ts")
                                          (invoke "lrelease" "MEGASync/gui/translations/MEGASyncStrings_es.ts")
                                          (invoke "lrelease" "MEGASync/gui/translations/MEGASyncStrings_fr.ts")
                                          (invoke "lrelease" "MEGASync/gui/translations/MEGASyncStrings_id.ts")
                                          (invoke "lrelease" "MEGASync/gui/translations/MEGASyncStrings_it.ts")
                                          (invoke "lrelease" "MEGASync/gui/translations/MEGASyncStrings_ja.ts")
                                          (invoke "lrelease" "MEGASync/gui/translations/MEGASyncStrings_ko.ts")
                                          (invoke "lrelease" "MEGASync/gui/translations/MEGASyncStrings_nl.ts")
                                          (invoke "lrelease" "MEGASync/gui/translations/MEGASyncStrings_pl.ts")
                                          (invoke "lrelease" "MEGASync/gui/translations/MEGASyncStrings_pt.ts")
                                          (invoke "lrelease" "MEGASync/gui/translations/MEGASyncStrings_ro.ts")
                                          (invoke "lrelease" "MEGASync/gui/translations/MEGASyncStrings_ru.ts")
                                          (invoke "lrelease" "MEGASync/gui/translations/MEGASyncStrings_th.ts")
                                          (invoke "lrelease" "MEGASync/gui/translations/MEGASyncStrings_vi.ts")
                                          (invoke "lrelease" "MEGASync/gui/translations/MEGASyncStrings_zh_CN.ts")
                                          (invoke "lrelease" "MEGASync/gui/translations/MEGASyncStrings_zh_TW.ts")

                                          (substitute* "MEGASync/mega/bindings/qt/sdk.pri"
                                            [("        error[(]\"Configuration file not found! Please re-run configure script located in the project's root directory!\"[)]") ""])))
                                      (add-before 'install 'set-install-location
                                        (lambda* (#:key outputs #:allow-other-keys)
                                          (let ([out (assoc-ref outputs "out")])
                                            (substitute* "MEGASync/Makefile"
                                              [("INSTALL_DIR   = cp -f -R") (string-append
                                                                              "INSTALL_DIR   = cp -f -R"
                                                                              "\n"
                                                                              "INSTALL_ROOT  = " out)]
                                              [("\\$[(]INSTALL_ROOT[)]/usr") "$(INSTALL_ROOT)"])))))))
      (home-page         "https://mega.io/sync/")
      (synopsis          "Lightweight calendar applet written in C++ using GTK")
      (description       "Gsimplecal is a lightweight calendar application written in
C++ using GTK.  Launched once, it pops up a small calendar applet, launched
again it closes the running instance.  It can additionally be configured to show
the current time in different timezones.")
      (license           license:bsd-3))))

(define-public megasync/wayland
  (package/inherit megasync
    (name "megasync-wayland")
    (inputs (modify-inputs (package-inputs megasync)
              (prepend qtwayland-5)))
    (arguments (substitute-keyword-arguments (package-arguments megasync)
                 ((#:phases phases) #~(modify-phases #$phases
                                        (add-after 'install 'wrap-qtwebengine-path
                                          (lambda* (#:key inputs outputs #:allow-other-keys)
                                            (wrap-program (search-input-file outputs "bin/megasync")
                                              `("QT_PLUGIN_PATH" = (,(string-append (assoc-ref inputs "qtwayland")
                                                                                    "/lib/qt5/plugins/"))))))))))))
