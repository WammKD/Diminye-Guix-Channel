(define-module (nondiminye qt)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build        utils)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (gnu  packages)
  #:use-module (gnu  packages     base)
  #:use-module (gnu  packages     bash)
  #:use-module (gnu  packages     bison)
  #:use-module (gnu  packages     cmake)
  #:use-module (gnu  packages     compression)
  #:use-module (gnu  packages     cups)
  #:use-module (gnu  packages     curl)
  #:use-module (gnu  packages     databases)
  #:use-module (gnu  packages     documentation)
  #:use-module (gnu  packages     enchant)
  #:use-module (gnu  packages     fontutils)
  #:use-module (gnu  packages     flex)
  #:use-module (gnu  packages     freedesktop)
  #:use-module (gnu  packages     elf)
  #:use-module (gnu  packages     gcc)
  #:use-module (gnu  packages     ghostscript)
  #:use-module (gnu  packages     gl)
  #:use-module (gnu  packages     glib)
  #:use-module (gnu  packages     gnome)
  #:use-module (gnu  packages     gnupg)
  #:use-module (gnu  packages     gperf)
  #:use-module (gnu  packages     graphics)
  #:use-module (gnu  packages     gstreamer)
  #:use-module (gnu  packages     gtk)
  #:use-module (gnu  packages     icu4c)
  #:use-module (gnu  packages     image)
  #:use-module (gnu  packages     jami)
  #:use-module (gnu  packages     kde-frameworks)
  #:use-module (gnu  packages     libevent)
  #:use-module (gnu  packages     linux)
  #:use-module (gnu  packages     llvm)
  #:use-module (gnu  packages     maths)
  #:use-module (gnu  packages     networking)
  #:use-module (gnu  packages     ninja)
  #:use-module (gnu  packages     nss)
  #:use-module (gnu  packages     pciutils)
  #:use-module (gnu  packages     pcre)
  #:use-module (gnu  packages     perl)
  #:use-module (gnu  packages     pkg-config)
  #:use-module (gnu  packages     pulseaudio)
  #:use-module (gnu  packages     protobuf)
  #:use-module (gnu  packages     python)
  #:use-module (gnu  packages     python-build)
  #:use-module (gnu  packages     python-web)
  #:use-module (gnu  packages     python-xyz)
  #:use-module (gnu  packages     qt)
  #:use-module (gnu  packages     regex)
  #:use-module (gnu  packages     ruby)
  #:use-module (gnu  packages     sdl)
  #:use-module (gnu  packages     serialization)
  #:use-module (gnu  packages     sqlite)
  #:use-module (gnu  packages     telephony)
  #:use-module (gnu  packages     tls)
  #:use-module (gnu  packages     valgrind)
  #:use-module (gnu  packages     video)
  #:use-module (gnu  packages     vulkan)
  #:use-module (gnu  packages     web-browsers)
  #:use-module (gnu  packages     xdisorg)
  #:use-module (gnu  packages     xiph)
  #:use-module (gnu  packages     xorg)
  #:use-module (gnu  packages     xml)
  #:use-module (srfi srfi-1))

(define-public libwidevinecdm
  (package
    (name          "libwidevinecdm")
    (version       "1.4.9.1088")
    (source        (origin
                     (method url-fetch)
                     (uri    (string-append
                               "https://dl.google.com/widevine-cdm/"
                               version
                               "-linux-x64.zip"))
                     (sha256 (base32
                               "1ipd5wlsidmh4j9n11s07137i2gnk7np2wgmn8irsmdqc61wmy0x"))))
    (build-system  copy-build-system)
    (native-inputs (list unzip patchelf))
    (inputs        `(("gcc:lib" ,gcc "lib")
                     ("glib"         ,glib)
                     ("nss"           ,nss)
                     ("nspr"         ,nspr)
                     ("glibc"       ,glibc)))
    (arguments     '(#:install-plan '(("libwidevinecdm.so" "lib/qt5/plugins/ppapi/"))
                     #:phases       (modify-phases %standard-phases
                                      (delete 'strip)
                                      (add-before 'validate-runpath 'fix-rpath
                                        (lambda* (#:key inputs outputs #:allow-other-keys)
                                          (let ([lib (string-append
                                                       (assoc-ref outputs "out")
                                                       "/lib/qt5/plugins/ppapi/libwidevinecdm.so")])
                                            (invoke "patchelf"
                                                    "--set-rpath"
                                                    (string-append 
                                                      (assoc-ref inputs "gcc:lib") "/lib"
                                                      ":"
                                                      (assoc-ref inputs "glib")    "/lib"
                                                      ":"
                                                      (assoc-ref inputs "nss")     "/lib/nss"
                                                      ":"
                                                      (assoc-ref inputs "nspr")    "/lib"
                                                      ":"
                                                      (assoc-ref inputs "glibc")   "/lib")
                                                    lib)))))))
    (home-page     "https://dl.google.com/widevine-cdm/current.txt")
    (synopsis      "Widevine library for Linux")
    (description   "I'm tired.")
    (license       license:unlicense)))

(define-public qutebrowser/wayland
  (package/inherit qutebrowser
    (name      "qutebrowser-wayland")
    (inputs    (modify-inputs (package-inputs qutebrowser)
                 (prepend qtwayland)))
    (arguments (substitute-keyword-arguments (package-arguments qutebrowser)
                 ((#:phases phases) #~(modify-phases #$phases
                                        (add-after 'wrap-qt-process-path 'wrap-qtwebengine-path
                                          (lambda* (#:key inputs outputs #:allow-other-keys)
                                            (wrap-program (search-input-file outputs "bin/qutebrowser")
                                              `("QT_PLUGIN_PATH" = (,(string-append (assoc-ref inputs "qtwayland")
                                                                                    "/lib/qt6/plugins/"))))))))))))

(define-public jami/wayland
  (package/inherit jami
    (name "jami-wayland")
    (inputs (modify-inputs (package-inputs jami)
              (prepend qtwayland)))
    (synopsis "Qt Jami client with Wayland rendering backend")))
