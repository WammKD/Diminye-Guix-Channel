(define-module (nondiminye wayland)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix     download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix build        utils)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system go)
  #:use-module (gnu  packages     autotools)
  #:use-module (gnu  packages     bash)
  #:use-module (gnu  packages     check)
  #:use-module (gnu  packages     cmake)
  #:use-module (gnu  packages     datastructures)
  #:use-module (gnu  packages     documentation)
  #:use-module (gnu  packages     elf)
  #:use-module (gnu  packages     freedesktop)
  #:use-module (gnu  packages     gl)
  #:use-module (gnu  packages     glib)
  #:use-module (gnu  packages     gtk)
  #:use-module (gnu  packages     image)
  #:use-module (gnu  packages     linux)
  #:use-module (gnu  packages     maths)
  #:use-module (gnu  packages     pciutils)
  #:use-module (gnu  packages     pkg-config)
  #:use-module (gnu  packages     pulseaudio)
  #:use-module (gnu  packages     qt)
  #:use-module (gnu  packages     video)
  #:use-module (gnu  packages     wm)
  #:use-module (gnu  packages     xdisorg)
  #:use-module (gnu  packages     xml)
  #:use-module (gnu  packages     xorg)
  #:use-module (guix gexp)
  #:use-module (gnu  packages     cpp)
  #:use-module (gnu  packages     golang)
  #:use-module (gnu  packages     gnome)
  #:use-module (gnu  packages     syncthing)
  #:use-module (gnu  packages     commencement))

;; Wayfire
(define wf-config/next
  (package/inherit wf-config
    (name    "wf-config")
    (version "0.9.0")
    (source  (origin
               (method    git-fetch)
               (uri       (git-reference
                            (url    "https://github.com/WayfireWM/wf-config/")
                            (commit (string-append "v" version))))
               (file-name (git-file-name name version))
               (sha256    (base32
                            "17ccv5wrv8yigkjc48gzyhb1py88wwj316kvkinc8kl2bfxa6xz4"))))))

(define-public wayfire
  (package
    (name           "wayfire")
    (version        "0.9.0")
    (source         (origin
                      (method git-fetch)
                      (uri    (git-reference
                                (url        (string-append "https://github.com/WayfireWM/wayfire"))
                                (recursive? #t)
                                (commit     (string-append "v" version))))
                      (sha256 (base32
                                "1qbbxsjys0cwhhwc04p6qnph0w21ir6yzr82cl985s1s87yph1n5"))))
    (build-system  meson-build-system)
    (native-inputs (list cmake
                         patchelf
                         ;; gcc-8
                         ;; For <filesystem> include: https://github.com/loot/libloot/issues/56#issuecomment-498404104
                               ;; Also could avoid this input and specify c++17 maybe: https://stackoverflow.com/a/39231488
                         gcc-toolchain-12
                         doxygen
                         doctest
                         pkg-config))
    (inputs        (list bash
                         glm
                         wayland
                         wayland-protocols
                         cairo
                         libdrm
                         mesa
                         libinput
                         libxkbcommon
                         xcb-util-renderutil
                         libevdev
                         wlroots-0.17
                         libxml2 ;; wf-config (git submodule)
                         libpng
                         libjpeg-turbo
                         wf-config/next
                         pango
                         nlohmann-json))
    (arguments
      (list #:configure-flags #~`(,(string-append "-Dcpp_args=-I"         (search-input-directory %build-inputs "/include/wayfire"))
                                  ,(string-append "-Dcpp_link_args=-ldl " (search-input-file      %build-inputs "/lib/libwlroots.so")
                                                  " "                     (search-input-file      %build-inputs "/lib/libwf-config.so")))
            #:phases          #~(modify-phases %standard-phases
                                  (add-after 'unpack 'patch-shell-path
                                    (lambda* (#:key inputs #:allow-other-keys)
                                      (substitute* "src/meson.build"
                                        (("/bin/sh") (string-append (search-input-file inputs "/bin/bash"))))
                                      (substitute* "src/core/core.cpp"
                                        (("/bin/sh") (string-append (search-input-file inputs "/bin/bash"))))))
                                  (add-before 'validate-runpath 'fix-runpath
                                    (lambda _
                                      (map (lambda (path)
                                             ;; using `invoke' causes an error of "No such file or directory"
                                             (system (string-append "patchelf --set-rpath "
                                                                    "\"" #$output "/lib:$(patchelf --print-rpath " #$output path ")\" "
                                                                    #$output path)))
                                           '("/bin/wayfire"
                                             "/lib/wayfire/libwindow-rules.so"
                                             "/lib/wayfire/libblur.so")))))))
    (home-page   "https://wayfire.org")
    (synopsis    "Wayland compositor")
    (description "Wayland compositor extendable with plugins.")
    (license     license:expat)))

(define-public wf-shell
  (package
    (name              "wf-shell")
    (version           "0.9.0")
    (source            (origin
                         (method url-fetch)
                         (uri    "https://github.com/WayfireWM/wf-shell/releases/download/v0.9.0/wf-shell-0.9.0.tar.xz")
                         (sha256 (base32
                                   "1d2bm4mn35hkh4mw5xnynb3qd95nnsa0651hsidzd956kydm5b68"))))
    (build-system      meson-build-system)
    (arguments         (list #:tests?          #f
                             #:glib-or-gtk?    #t
                             ;; >>> MALLOC_PERTURB_=68 /gnu/store/jh59fh48mcffyz5wmsjj0p96xkkflbz0-python-wrapper-3.10.7/bin/python /tmp/guix-build-wf-shell-0.8.1.drv-0/wf-shell-0.8.1/subprojects/gtk-layer-shell/test/tests-not-enabled.py
                             ;; ――――――――――――――――――――――――――――――――――――― ✀  ―――――――――――――――――――――――――――――――――――――
                             ;; you must run [32;1mmeson build --reconfigure -Dtests=true[0m in order to run the tests (where build is the path to your build directory)
                             ;; ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
                             ;; #:configure-flags #~(list "-Dtests=true")
                             ))
    (native-inputs     (modify-inputs (package-native-inputs wlroots-0.17)
                         (prepend cmake)
                         (prepend pkg-config)
                         (prepend `(,glib "bin"))))  ; for glib-mkenums
    (inputs            (modify-inputs (package-inputs wf-config/next)
                         (prepend gtkmm-3)
                         (prepend gobject-introspection)
                         (prepend pulseaudio)
                         (prepend alsa-lib)
                         (prepend vala)
                         (prepend libdbusmenu)
                         (prepend wf-config/next)))
    (propagated-inputs (list wayfire))
    (home-page         "https://wayfire.org")
    (synopsis          "Panel, dock and background applications for wayfire")
    (description       synopsis)
    (license           license:expat)))



;; Wbg
(define-public wbg
  (package
    (name          "wbg")
    (version       "1.1.0")
    (source        (origin
                     (method git-fetch)
                     (uri    (git-reference
                               (url    (string-append "https://codeberg.org/dnkl/" name "/"))
                               (commit version)))
                     (sha256 (base32
                               "0rq2m2cddggmxq77pfks1mga3y7a54pnm2linqipia1l4ylhi4i4"))))
    (build-system  meson-build-system)
    (native-inputs (list pkg-config cmake tllist wayland-protocols))
    (inputs        (list pixman wayland libpng libjpeg-turbo))
    (synopsis      "Box style window manager")
    (description
     "Openbox is a highly configurable, next generation window manager with
extensive standards support.  The *box visual style is well known for its
minimalistic appearance.  Openbox uses the *box visual style, while providing
a greater number of options for theme developers than previous *box
implementations.")
    (home-page    "http://openbox.org/wiki/Main_Page")
    (license      license:gpl2+)))



;; wl-clipboard
(define-public wl-clipboard/diminye
  (package/inherit wl-clipboard
    (name    "wl-clipboard--diminye")
    (version "2.2.1")
    (source  (origin
               (method    git-fetch)
               (uri       (git-reference
                            (url    "https://github.com/bugaevc/wl-clipboard")
                            (commit (string-append "v" version))))
               (file-name (git-file-name name version))
               (sha256    (base32
                            "09l6dv3qsqscbr98vxi4wg4rkx0wlhnsc40n21mx3ds6balmg105"))))))
